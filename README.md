# SSDI Dataviz Project

## Purpose
As a part of a larger project, I'm attempting to compare the publicly available Social Security Disability Insurance
data to the cost of living, and rent costs from that year. The goal is to demonstrate the relationship between poverty 
and disability, and potentially how the Government knowingly keeps disabled people (and their families) in poverty.

## Abbreviations
- SSA - (United States) Social Security Administration
- SSDI - Social Security Disability Insurance (in this case, this represents the disability payment program)
- HUD - (United States Department of) Housing and Urban Development
- FMR - Fair Market Rent (as determined by HUD)

## Usage
Run `main.py`, which will ask you for a data download directory. Download directory can be left blank, which will 
default to `$PWD/data/`. The location you provide will be created if it doesn't already exist.

The file will then ask if you want to download all three datasources (which are required for the dataviz to work). It 
will automatically skip files that have already been downloaded, to avoid wasting time.

## TODO:
- [X] Get the scraper working for a single year
- [X] Get the scraper working for all publicly available years
- [X] Save data as CSV, so that it doesn't pull the data every time the code runs
- [X] Find external, high quality\* cost of rent data
- [ ] Create static dataviz to compare the datasets
- [ ] Create dynamic dataviz to compare the datasets

> \* see the [HUD](#hud) section for more caveats on the phrase "high quality"

## Data Sources
### SSA
For this project, we pull data from three sources. Thankfully, two of them are straight from the SSA, which means that 
we've got the actual population, rather than sample data. The data saved in `.../data/di_by_sex_age_dollar_amount` 
contains scraped CSVs from the SSA, which contain population data for SSDI payouts organized by dollar amount, age, and 
sex. Each year is saved to a different file.

The data saved in `.../data/di_by_state` is also from the SSA, and represents the number of DI recipients by U.S. state 
and county.

### HUD
The data saved in `.../data/fmr` is from HUD. There is a 1985-current year historical CSV available, which the program
is hardcoded to download (if it hasn't already been done!) This is used to determine rent costs per county. This is not
a perfect way to represent rent costs - it specifically represents 40th percentile rent costs per county. However, this 
is the most rigorous dataset that we currently have access to. 
> **NOTE:** If you are able to find a better, more representative dataset for true rent costs per county per year, 
> please submit a PR!