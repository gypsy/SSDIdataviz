import scraper

# create a list of all years between 2006, which was the first date I could find data from on their site, to the current
# year (2023 as of last edit). Though this is being created on New Year's Eve 2023, there isn't a 2024 yet available. I
# have no idea when that may become available, so this will have to do (for now).
years = list(range(2006, 2023 + 1))

# NOTE: All three functions will return early if they detect that the file they are attempting to download already
# exists.

# These two for loops have been separated to limit the amount of working memory they eat. They don't seem to write their
# files to disk until the for loop stops iterating.
for year in years:
    scraper.scrape_ssdi_by_year(year)

for year in years:
    scraper.download_regional_oasdi_xlsx_files()

# download the FMR dataset. This is a single file, and it's a one shot, so it's quick.
scraper.download_fmr_by_year()
