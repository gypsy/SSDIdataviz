import os
import pandas as pd
import requests
from bs4 import BeautifulSoup
import wget
import json

STATE_CODES = [
        'al',
        'ak',
        'az',
        'ar',
        'ca',
        'co',
        'ct',
        'de',
        'fl',
        'ga',
        'hi',
        'id',
        'il',
        'in',
        'ia',
        'ks',
        'ky',
        'la',
        'me',
        'md',
        'ma',
        'mi',
        'mn',
        'ms',
        'mo',
        'mt',
        'ne',
        'nv',
        'nh',
        'nj',
        'nm',
        'ny',
        'nc',
        'nd',
        'oh',
        'ok',
        'or',
        'pa',
        'ri',
        'sc',
        'sd',
        'tn',
        'tx',
        'ut',
        'vt',
        'va',
        'wa',
        'wv',
        'wi',
        'wy'
    ]

def download_regional_oasdi_xlsx_files(scrape_year):

    # the URL is hardcoded. This isn't great practice, but it works for now.
    url = 'https://www.ssa.gov/policy/docs/statcomps/oasdi_sc/'

    # Get the current working directory.
    cwd = os.getcwd()

    # set path to $CWD/data/xlsx, and create the directory if it doesn't already exist
    path = f'{cwd}/data/di_by_state/'
    if not os.path.exists(path):
        os.makedirs(path)

    print(f'Now scraping {scrape_year}...')

    # iterate through the 50 state codes, and download the xlsx file. Originally, I was going to programmatically
    # collect all XLSX files on the page, but there are a bunch of other files not related to our project.
    for state in STATE_CODES:

        filename = f'{path}/{state}-{scrape_year}'

        # if the file already exists, skip the download. wget will create duplicates (which are annoying)
        if not (os.path.exists(filename)):
            wget.download(f'{url}{scrape_year}/{state}.xlsx', out=filename)
        else:
            print(f'{filename} already appears to exist, skipping file...\n')

    print(f'Completed scraping {scrape_year}!\n')


def scrape_ssdi_by_year(scrape_year):
    url = f'https://www.ssa.gov/OACT/ProgData/benefits/da_mbc{scrape_year}06.html'

    # Get the current working directory, to save the data into a CSV. Set `path` to the directory we
    cwd = os.getcwd()
    path = f'{cwd}/data/di_by_sex_age_dollar_amount'

    # if the file for the current year already exists, return early.
    if os.path.exists(f'{path}/{scrape_year}06.csv'):
        print(f'File {path}/{scrape_year}06.csv already appears to exist. Returning early!')
        return

    # create a response using requests, create a BeautifulSoup object using the response.
    response = requests.get(url).text
    soup = BeautifulSoup(response, "html.parser")

    # extract all tables, and extract all rows from all tables
    table = soup.find('table')
    rows = table.find_all("tr")

    # strip the HTML tags and get the raw data out of the 'rows' variable convert it to a dataframe, with pre-created
    # column headers.
    data = [[td.text.strip() for td in row.find_all("td")] for row in rows[1:]]
    dataframe = pd.DataFrame(data,
                             columns=[
                                 "Benefit level",
                                 "Total, All Ages",
                                 "Under 40",
                                 "40-49",
                                 "50-59",
                                 "60 and Over"
                             ])

    # Add a column to the end of the table with the date, as derived from year variable
    dataframe['Date'] = f'{scrape_year}-06'

    # identify all locations of 'None' in column 'Benefit level', these are invalid rows.
    dfDropIndexes = dataframe.loc[dataframe['Benefit level'].isnull()].index.tolist()
    # drop invalid rows
    dataframe.drop(index=dfDropIndexes, inplace=True)
    # this code resets the indexes in the dataframe back to standard numerical order, as they don't automatically update
    # after dropping rows. The `inplace=True` is required to affect the original object, otherwise nothing will appear
    # to happen.
    dataframe.reset_index(drop=True, inplace=True)

    # store the starting indexes of each of the three tables from the dataset. We'll drop the first table (Both sexes),
    # as it's just the combined values of the next two (Male, Female).
    tableStartList = dataframe.loc[dataframe['Benefit level'] == 'Less than $100'].index.tolist()

    # Find the beginning and end of the first table. The tables appear on the site in this order: Combined Sexes, Male,
    # Female. Since the first table is redundant, we're dropping it from the dataframe outright.
    firstTableStartIndex = tableStartList[0]
    firstTableEndIndex = (tableStartList[1]) - 1
    dataframe.drop(index=dataframe.loc[firstTableStartIndex:firstTableEndIndex].index,
                   inplace=True
                   )
    # this code resets the indexes in the dataframe back to standard numerical order, as they don't automatically update
    # after dropping rows.
    dataframe.reset_index(drop=True, inplace=True)

    # create 'Sex' column, set it to an empty string.
    dataframe['Sex'] = ""

    # assign the 'Male' and 'Female' values to the correct parts of the data, per the original table
    dataframe.loc[tableStartList[0]:(tableStartList[1] - 1), 'Sex'] = 'Male'
    dataframe.loc[tableStartList[1]:(tableStartList[2] - 1), 'Sex'] = 'Female'

    # Check that the export folder exists, create it if not.
    if not os.path.exists(path):
        os.makedirs(path)
    # Export the dataframe to a CSV file in the `path` directory
    dataframe.to_csv(f'{path}/{scrape_year}06.csv')


def download_fmr_by_year():
    # the entire historical dataset, which we will process here
    url = 'https://www.huduser.gov/portal/datasets/FMR/FMR_All_1983_2024.csv'

    # Get the current working directory.
    cwd = os.getcwd()

    # set path to $CWD/data/xlsx, and create the directory if it doesn't already exist
    path = f'{cwd}/data/fmr/'
    if not os.path.exists(path):
        os.makedirs(path)

    print(f'Now downloading FMR historical CSV...')

    filename = f'{path}fmr-historical.csv'
    # if the file already exists, skip the download. wget will create duplicates (which are annoying)
    if not (os.path.exists(filename)):
        wget.download(url, out=filename)
    else:
        print(f'{filename} already appears to exist, skipping download...\n')
